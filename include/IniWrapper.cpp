/*
Copyright (c) 2017-2018, Schadin Alexey (schadin@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
and the following disclaimer in the documentation and/or other materials provided with
the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <filesystem>

#include "IniWrapper.h"

IniWrapper::IniWrapper(const std::wstring &file_name) : file_name_{ file_name }
{
    if (!std::experimental::filesystem::exists(file_name)) {
        HANDLE hFile = CreateFile(file_name.c_str(), GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0);
        if (hFile != INVALID_HANDLE_VALUE) {
            CloseHandle(hFile);
        }
    }
}

std::wstring IniWrapper::readStringValue(std::wstring & section, std::wstring & key, std::wstring def) {
    const int MaxSize = 8000;
    TCHAR *value = new TCHAR[MaxSize + 1];
    GetPrivateProfileString(section.c_str(), key.c_str(), def.c_str(), value, MaxSize, file_name_.c_str());
    std::wstring result = std::wstring(value);
    delete[] value;

    return result;
}

bool IniWrapper::writeStringValue(std::wstring & section, std::wstring & key, std::wstring & value) {
    return WritePrivateProfileString(section.c_str(), key.c_str(), value.c_str(), file_name_.c_str());
}

int IniWrapper::readIntValue(std::wstring & section, std::wstring & key, int def) {
    return GetPrivateProfileInt(section.c_str(), key.c_str(), def, file_name_.c_str());
}

bool IniWrapper::writeIntValue(std::wstring & section, std::wstring & key, int & value) {
    std::wstring str_val = std::to_wstring(value);
    return writeStringValue(section, key, str_val);
}

