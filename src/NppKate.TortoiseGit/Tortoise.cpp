/*
Copyright (c) 2017-2018, Schadin Alexey (schadin@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
and the following disclaimer in the documentation and/or other materials provided with
the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <filesystem>
#include <sstream>
#include <Windows.h>
#include <shellapi.h>
#include <KnownFolders.h>
#include <ShlObj.h>
#include <strutils.h>
#include <VersionHelpers.h>
#include "Tortoise.h"

namespace fs = std::experimental::filesystem;

bool check_path(fs::path path, std::wstring &str_path) {
    path.append(L"TortoiseGit").append(L"bin").append(L"TortoiseGitProc.exe");

    if (fs::exists(path)) {
        str_path = path.parent_path().wstring();
        return true;
    }
    return false;
}

Tortoise::Tortoise(NppUtils &npp_utils, Config &config) : npp_utils_{ npp_utils }, config_ { config } { 
    if (config.tortoise_proc_path().empty() && config.is_first_run()) {
        // search tortoise git
        /*
        �������� ������
        OS    Npp   TortoiseGit
        32    32    Program Files
        64    32    Program Files; Program Files (x86)
        64    64    Program Files; Program Files (x86)
        */
        auto program_files = fs::path{};
        auto program_files_x86 = fs::path{};
        TCHAR buffer[MAX_PATH];
#ifndef _WIN64
        // 32bit process
        BOOL isWow64 = 0;

        IsWow64Process(GetCurrentProcess(), &isWow64);
        if (isWow64) {
            // OS 64
            if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, buffer))) {
                program_files_x86 = fs::path{ buffer };

                auto str_path = program_files_x86.wstring();
                program_files = fs::path{ str_path.substr(0, str_path.size() - 6) };
            }
        }
        else 
        {
            if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, buffer))) {
                program_files = fs::path{ buffer };
            }
        }
#else
        // 64bit process
        // CSIDL_PROGRAM_FILES
        // CSIDL_PROGRAM_FILESX86
        if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, buffer))) {
            program_files = fs::path{ buffer };
        }
        if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, buffer))) {
            program_files_x86 = fs::path{ buffer };
        }
#endif // __MACHINEX86
        std::wstring str_path{};
        if (!program_files.empty() && check_path(program_files, str_path)) {
            config.set_tortoise_proc_path(str_path);
        }
        else if (!program_files_x86.empty() && check_path(program_files_x86, str_path)) {
            config.set_tortoise_proc_path(str_path);
        }
    }
}


Tortoise::~Tortoise() { }

void Tortoise::tortoise_command(const std::wstring command, const std::wstring path, std::map<std::wstring, std::wstring> params, CloseOnEnd close_end)
{
    const std::wstring space = __TEXT(" ");
    auto ss = std::wstringstream{};
    auto exe_path = fs::path(config_.tortoise_proc_path()).append(__TEXT("TortoiseGitProc.exe"));

    ss << __TEXT("/command:") << command << space;
    if (!path.empty()) {
        ss << _TEXT("/path:") << path << space;
    }
    for each (auto param in params) {
        ss << __TEXT("/") << param.first << __TEXT(":\"") << param.second << __TEXT("\"") << space;
    }
    ss << __TEXT("/closeonend:") << close_end;

    ShellExecute(npp_utils_.npp_handle(), NULL, exe_path.c_str(), ss.str().c_str(), NULL, SW_SHOW);
}

std::wstring Tortoise::git_path()
{
    std::wstring result{};

    auto path = fs::path(npp_utils_.current_file_path()).parent_path();
    while (!path.empty() && path.has_root_path()) {
        auto git = fs::path(path).append(".git");
        if (fs::exists(git)) {
            result = path.wstring();
            break;
        }
        path = path.parent_path();
    }
    return result;
}


void Tortoise::pull()
{
    tortoise_command(L"pull", git_path(), {}, CloseManual);
}

void Tortoise::fetch()
{
    tortoise_command(L"fetch", git_path(), {}, CloseManual);
}

void Tortoise::push()
{
    tortoise_command(L"push", git_path(), {}, CloseManual);
}

void Tortoise::add()
{
    tortoise_command(L"add", npp_utils_.current_file_path(), {}, CloseManual);
}

void Tortoise::blame()
{
    tortoise_command(L"blame", npp_utils_.current_file_path(), {}, CloseManual);
}

void Tortoise::blame_line()
{
    tortoise_command(L"blame", npp_utils_.current_file_path(), { std::make_pair(L"line", std::to_wstring(npp_utils_.current_line())) }, CloseManual);
}

void Tortoise::commit()
{
    tortoise_command(L"commit", git_path(), {}, CloseManual);
}

void Tortoise::repostatus()
{
    tortoise_command(L"repostatus", git_path(), {}, CloseManual);
}

void Tortoise::rebase()
{
    tortoise_command(L"rebase", git_path(), {}, CloseManual);
}

void Tortoise::log()
{
    tortoise_command(L"log", git_path(), {}, CloseManual);
}

void Tortoise::log_file()
{
    tortoise_command(L"log", npp_utils_.current_file_path(), {}, CloseManual);
}

void Tortoise::stash_save()
{
    tortoise_command(L"stashsave", git_path(), {}, CloseManual);
}

void Tortoise::stash_apply()
{
    tortoise_command(L"stashapply", git_path(), {}, CloseManual);
}

void Tortoise::stash_pop()
{
    tortoise_command(L"stashpop", git_path(), {}, CloseManual);
}

void Tortoise::rename()
{
    tortoise_command(L"rename", git_path(), {}, CloseManual);
}

void Tortoise::remove()
{
    tortoise_command(L"remove", git_path(), {}, CloseManual);
}

void Tortoise::checkout()
{
    tortoise_command(L"switch", git_path(), {}, CloseManual);
}

void Tortoise::merge()
{
    tortoise_command(L"merge", git_path(), {}, CloseManual);
}

void Tortoise::settings()
{
    tortoise_command(L"settings", L"", {}, CloseManual);
}

void Tortoise::select_bin_folder()
{
    std::wstring bin_path{};
    if (IsWindowsVistaOrGreater()) {
        HRESULT hr;
        IFileOpenDialog *pOpenFolderDialog;
        // CoCreate the dialog object.
        hr = CoCreateInstance(CLSID_FileOpenDialog,
            NULL,
            CLSCTX_INPROC_SERVER,
            IID_PPV_ARGS(&pOpenFolderDialog));

        if (SUCCEEDED(hr))
        {
            pOpenFolderDialog->SetOptions(FOS_PICKFOLDERS);
            pOpenFolderDialog->SetTitle(L"�������� ����� Tortoise\\bin");

            // Show the dialog
            hr = pOpenFolderDialog->Show(NULL);

            if (SUCCEEDED(hr))
            {
                // Obtain the result of the user's interaction with the dialog.
                IShellItem *psiResult;
                hr = pOpenFolderDialog->GetResult(&psiResult);

                if (SUCCEEDED(hr))
                {
                    // Do something with the result.
                    LPWSTR pwsz = NULL;

                    hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pwsz);

                    if (SUCCEEDED(hr)) {
                        bin_path.append(pwsz);
                        CoTaskMemFree(pwsz);
                    }
                    psiResult->Release();
                }
            }
            pOpenFolderDialog->Release();
        }
    }
    else {
        TCHAR buffer[MAX_PATH];
        BROWSEINFO browserInfo{};
        PCIDLIST_ABSOLUTE pidl;
        browserInfo.hwndOwner = npp_utils_.npp_handle();
        browserInfo.pidlRoot = NULL;
        browserInfo.pszDisplayName = (LPWSTR)&buffer;
        browserInfo.lpszTitle = L"�������� ����� Tortoise\\bin";
        browserInfo.ulFlags = BIF_NONEWFOLDERBUTTON;
        pidl = SHBrowseForFolder(&browserInfo);
        if (pidl) {
            SHGetPathFromIDList(pidl, (LPWSTR)&buffer);
            bin_path.append(buffer);
        }

    }

    if (!bin_path.empty()) {
        config_.set_tortoise_proc_path(bin_path);
    }
}
